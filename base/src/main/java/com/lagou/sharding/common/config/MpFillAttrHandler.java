package com.lagou.sharding.common.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class MpFillAttrHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("MP: 插入 属性值自动填充 ....");
        this.strictInsertFill(metaObject, "createTime", Date.class, new Date());
        this.strictInsertFill(metaObject, "updateTime", Date.class, new Date());

    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("MP: 更新 属性值自动填充 .... ....");
        this.strictUpdateFill(metaObject, "updateTime", Date.class, new Date());


    }
}
