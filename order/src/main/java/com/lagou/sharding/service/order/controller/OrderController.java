package com.lagou.sharding.service.order.controller;

import cn.hutool.core.util.RandomUtil;
import com.lagou.sharding.business.model.Order;
import com.lagou.sharding.common.pojo.ResultInfo;
import com.lagou.sharding.service.order.service.impl.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping
public class OrderController {

    @Autowired
    private OrderServiceImpl orderService;

    /**
     * http://127.0.0.1:8080/order/save/1000/wait-0
     * http://127.0.0.1:8080/order/save/1001/wait-1
     * @param companyId
     * @param status
     * @return
     */
    @GetMapping("/save/{companyId}/{status}")
    public ResultInfo<Boolean> save(@PathVariable Integer companyId, @PathVariable String status) {
        return orderService.saveOrder(new Order()
                .setCompanyId(companyId)
                .setStatus(status)
                .setPositionId(RandomUtil.randomInt(1,10))
                .setPublishUserId(RandomUtil.randomInt(10000000,10000010)));
    }

    /**
     * http://127.0.0.1:8080/order/one/1380174111419097090
     * @param orderId
     * @return
     */
    @GetMapping("/one/{orderId}")
    public ResultInfo<Order> getOne(@PathVariable Long orderId) {
        return orderService.oneOder(orderId);
    }

    /**
     * http://127.0.0.1:8080/order/list/publish/user/10000006
     * @param publishUserId
     * @return
     */
    @GetMapping("/list/publish/user/{publishUserId}")
    public ResultInfo<List<Order>> getByPubUserId(@PathVariable Integer publishUserId){
        return orderService.findByPubUserId(publishUserId);
    }

}
